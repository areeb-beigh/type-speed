# NOTE
**My projects are active only on GitHub, these are only the initial releases on BitBucket. GitHub: https://github.com/areeb-beigh**

## Python Type-Speed Tester ###
This is a small application that I made, well, for fun. It contains about 1800 different words extracted from a Sherlock Holmes' novel [(Using using my word extractor)](http://www.areeb-beigh.tk/my_works/python/index.html). It will choose a random word from the entire list and you'll have to type that in. Repeat the same for 120 seconds (2 minutes) & at the end you'll receive a statistics of your typing speed.

I've also compiled the program into a .exe version [(using py2exe)](http://py2exe.org/) so that everyone can use it. You can find the setup.py file to compile the program in the Python directory. To compile simply execute **"python setup.py py2exe"**** in the terminal. (You must have py2exe module installed).

If you're a windows user and you don't have Python installed you can simply open the TypeSpeed.exe file in the Windows folder included in the package. Others can, of course, simply use the pure Python version in the Python directory.

### Possible Snags ###

1. Missing files (Windows)

If you're getting some errors such as "Blabla.dll file is missing from your computer" then make sure you have your windows system files installed correctly also make sure that you download the file from the correct location. An up to date link to the respository will always be available on my website [here](http://www.areeb-beigh.tk/my_works/python).

If by any chance the above link is broken you can also get the download from my GitHub profile [here](https://github.com/areeb-beigh/Type-Speed).

### 2. Spellings ###

Since there are over 1800 words in the program I couldn't check all of them. So if you find any miss spelled word in the program please inform me through any of the contacts [here](http://areeb-beigh.tk/contact.html).

### 3. Bugs ###

If you find any bugs in the program, again feel free to contact me through any of the contacts [here](http://areeb-beigh.tk/contact.html).

Required modules (Linux / Python)

To run the program smoothly Python / Linux users will require the following modules (why you probably would have installed with Python by default):

* random
* threading
* webbrowser
* sys
* time

### Other stuff ###

Thanks to Emil Bajrovski for beta testing on Windows 8 and pointing out a bug.

* **Developer:** Areeb Beigh
* **Website:** [www.areeb-beigh.tk](www.areeb-beigh.tk)
* **Mail:** areebbeigh@gmail.com
* **Version:** 1.0